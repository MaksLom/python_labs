import math

a = float(input('Enter a: '))
x = float(input('Enter x: '))

g = (5 * ((-10) * a ** 2 + 27 * a * x + 28 * x ** 2)) / (5 * a ** 2 - 9 * a * x + 4 * x ** 2)
f = math.cos(20 * a ** 2 - 57 * a * x + 40 * x ** 2)
y = math.log(10 * a ** 2 + 13 * a * x + 3 * x ** 2 + 1)

print('Function g = ', g)
print('Function f = ', f)
print('Function y = ', y)

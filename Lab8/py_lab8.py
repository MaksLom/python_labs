import random
import matplotlib.pyplot as plt
import matplotlib.patches as pch


def area_for_Py_Lab8(point, center, radius):
    return (float(center[0]) - float(point[0])) ** 2 + (float(center[1]) - float(point[1])) ** 2 <= radius ** 2


def counts(points, center, radius):
    count = 0
    for point in points:
        if area_for_Py_Lab8(point, center, radius):
            count += 1
    return count


n = int(input('Number of points : '))
radius = float(input('Radius : '))

points = [(random.uniform(-100, 100), random.uniform(-100, 100)) for i in range(n)]
center = (random.uniform(-100, 100), random.uniform(-100, 100))

points_q = counts(points, center, radius)
print(f'Number of points in the circle : {points_q}')

ax = plt.subplot()
ax.scatter(center[0], center[1], c='mediumblue', s=3, zorder=100, marker='X')
ax.scatter([x for x, y in points if area_for_Py_Lab8((x, y), center, radius) is True],
           [y for x, y in points if area_for_Py_Lab8((x, y), center, radius) is True],
           s=2,
           c='orangered',
           zorder=50,
           marker='p')
ax.scatter([x for x, y in points if area_for_Py_Lab8((x, y), center, radius) is False],
           [y for x, y in points if area_for_Py_Lab8((x, y), center, radius) is False],
           s=1,
           c='lime',
           zorder=0)

ax.add_patch(pch.Circle(center, radius=radius, fill=False))
ax.axis('square')
plt.show()

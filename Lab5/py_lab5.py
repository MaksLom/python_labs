import math
import sys
import matplotlib.pyplot as plt


def plot_data(x, y, legend='f(x)', title='Plot f(x)', x_label='x', y_label='f(x)'):
    plt.plot(x, y, 'ko-')
    plt.legend([legend])
    plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.show()
    function_max = max(y)
    function_min = min(y)
    print('f(x) maximum = ', function_max, '-', 'f(x) minimum = ', function_min)


while True:
    x_mass, g_mass, f_mass, y_mass = [], [], [], []

    try:
        a = float(input('Enter a: '))
        x = float(input('Lower bound of the x: '))
        x_l = float(input('Upper bound of the x: '))
        if x >= x_l:
            print('Input error')
            sys.exit(1)
        function = int(input('Select function(1 - g, 2 - f, 3 - y): '))
        n_s = int(input('Enter number of steps: '))
        find = input('f(x) that can be in the results: ')
    except ValueError:
        print('Input error')
        sys.exit(1)

    step = (x_l - x) / n_s

    if function in range(1, 4):

        if function == 1:
            while x <= x_l:
                try:
                    g = (5 * ((-10) * a ** 2 + 27 * a * x + 28 * x ** 2)) / (5 * a ** 2 - 9 * a * x + 4 * x ** 2)
                except ArithmeticError:
                    print('x =', x, '-', 'Error(div zero)')
                    g_mass.append(None)
                    x_mass.append(x)
                else:
                    print('x =', x, '-', 'Function g =', g)
                    g_mass.append(g)
                    x_mass.append(x)
                x += step
            plot_data(x_mass, g_mass, 'g(x)', 'Plot g(x)', 'x', 'g(x)')

        elif function == 2:
            while x <= x_l:
                f = math.cos(20 * a ** 2 - 57 * a * x + 40 * x ** 2)
                print('x =', x, '-', 'Function f =', f)
                x += step
                f_mass.append(f)
                x_mass.append(x)
            plot_data(x_mass, f_mass, 'f(x)', 'Plot f(x)', 'x', 'f(x)')

        elif function == 3:
            while x <= x_l:
                try:
                    y = math.log(10 * a ** 2 + 13 * a * x + 3 * x ** 2 + 1)
                except ValueError:
                    print('x =', x, '-', 'ValueError')
                    y_mass.append(None)
                    x_mass.append(x)
                else:
                    print('x =', x, '-', 'Function y =', y)
                    y_mass.append(y)
                    x_mass.append(x)
                x += step
            plot_data(x_mass, y_mass, 'y(x)', 'Plot y(x)', 'x', 'y(x)')
    else:
        print('Error')
        sys.exit(1)

    rs = list(map(str, g_mass or f_mass or y_mass))
    print('Results: {0}'.format(', '.join(rs)))

    print('Number of (results = find):', rs.count(find))

    while True:
        again = input("Try again? Input 'Yes', 'Y' or 'y': ")
        if again in ['Yes', 'Y', 'y']:
            break
        else:
            sys.exit(0)

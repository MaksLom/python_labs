import math
import sys

a = float(input('Enter a: '))  # Ввод a
x = float(input('Enter x: '))  # Ввод x
function = int(input('Select function(1 - g, 2 - f, 3 - y): '))  # Выбор функции

if function in range(1, 4):

    if function == 1:
        try:
            g = (5 * ((-10) * a ** 2 + 27 * a * x + 28 * x ** 2)) / (5 * a ** 2 - 9 * a * x + 4 * x ** 2)
            # Попытка вычислить G
        except ArithmeticError:
            print('Error(div zero)')  # Вывод ошибки
            sys.exit(1)
        else:
            print('Function g =', g)  # Вывод G

    elif function == 2:
        f = math.cos(20 * a ** 2 - 57 * a * x + 40 * x ** 2)  # Попытка вычислить F
        print('Function f =', f)  # Вывод F

    elif function == 3:
        try:
            y = math.log(10 * a ** 2 + 13 * a * x + 3 * x ** 2 + 1)  # Попытка вычислить Y
        except ValueError:
            print('ValueError')  # Вывод ошибки
            sys.exit(1)
        else:
            print('Function y =', y)  # Вывод Y

else:
    print('Error')
    sys.exit(1)
